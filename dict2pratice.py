#printing of dict by for loop
thisdict={
    "brand":"ford",
    "model":"mustang",
    "year": 1984
}
for x in thisdict:
    #print all key name one by one
    print(x)
    #print all value name one by one
    print(thisdict[x])
    